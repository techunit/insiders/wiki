---
title: "Gatling"
weight: 102
---


Gatling est une application permettant la gestion des tests de charge. A l'inverse de [Apache JMeter](https://jmeter.apache.org/), Gatling est plus cohérente à utiliser pour les devs (dans le cadre de CI/CD) mais moins pertinente à la QA.

## Installation

1. [Installer Java > 1.18](https://java.com/fr/download/manual.jsp)
1. [Installer Scala > 2.12](https://www.scala-sbt.org/download.html). Il s'agit du CLI pour executer des scripts Scala, langage utilisé par Gatling pour programmer les tests
1. [Installer l'application Gatling](https://gatling.io/open-source/). Il s'agit d'avoir une interface utilisateur pour configurer/executer les test
1. Assurez-vous d'avoir un IDE avec une extension Scala afin de faciliter la rédaction des scripts