---
title: "Micro services"
weight: 101
---


# Wiki techUnit

*Découvrir, apprendre, partiquer puis partager*

**{{< techunit >}} a pour vocation d'être une organisation sans but lucratif, destinée à ceux qui veulent aller au-delà de leur connaissances et étudier les dernières technologies ensemble.**

**Il est essentiel pour nous de savoir échanger et confronter nos idées pour en tirer le meilleurs aux travers de différents projets**

### L'objectif de ce wiki est d'être notre base de connaissance, il contiendra :

* Des présentations d'outils et technologie ainsi que les informations qui seront le plus utilisées dans le carde de {{< techunit >}}
* Les codes de conduites & procédures techniques
* Les supports des workshop

### Pour contribuer

1. Prendre connaissance des [Codes de conduite](./_conductcode)
1. Découvrir [Hugo & le markdown](./hugo)