---
title: "Command lines reminder"
weight: 1000
---

## Docker commands

## Kubernetes commands

Role|Command|Description
:-:|:-:|:-:
List cluster connection|`kubectl config get-contexts`|
Use cluster connection|`kubectl config use-context <cluster-name>`|
Delete cluster connection|`kubectl config delete-context <cluster-name>`|
Create pod & port mapping from a Docker image|`kubectl run <pod-name> --generator=run-pod/v1 --image=<docker-image> --port=<port>`|
Create deployment & pod from a Docker image|`kubectl create deployment <deploy-name> --image=<docker-image>`|
Create service listening on 8080|`kubectl expose deployment <deploy-name> --type=<ServiceType> --port=8080`|ServiceType can be ClusterIP, NodePort, LoadBalancer or ExternalName
Apply K8S configuration speicifed in a `kustomization.yaml` file|`kubectl apply -k .`|
Delete K8S configuration speicifed in a `kustomization.yaml` file|`kubectl delete -k .`|
Apply K8S configuration in the specified file|`kubectl apply -f <filePath>`|`filePath` can be an web url or a local path|
Apply all K8S configurations in yaml, yml & json files in directory|`kubectl apply -f <directory>`|
List cluster events|`kubectl get events`|
List cluster pods in all namespaces|`kubectl get pod --all-namespaces`|
List cluster pods and display more data|`kubectl get pod -o wide`|
List cluster pods in default's namespace|`kubectl get pod --namespace=default`|
List cluster pods and display related labels *app* and *tiers*|`kubectl get pod -Lapp -Ltier`|
List cluster pods with labels *app* equals *App01* and *tiers* equals *Frontend* |`kubectl get pod -l app=App01,tier=Frontend`|
Show detailed information about a resource|`kubectl describe <resource-type> <resource-name>`|
Set a label for a pod|`kubectl label pod <pod-name> <label-name>=<label-value>`|
Print the logs from a container in a pod|`kubectl logs <pod-name>`|
Execute a command on a container in a pod|`kubectl exec <pod-name> env`|Lists the environment variables
Execute a command on a specific container in a pod|`kubectl exec -it <pod-name> --container <container-name> env`|Lists the environment variables
Execute a command on a container in a pod|`kubectl exec -it <pod-name> -- /bin/bash`|Opens an interactive shell, the double dash “–” disctincts kubectl's arguments
List cluster pods name|`kubectl get pods -o Name`|
List cluster services|`kubectl get svc`|
List cluster deployments|`kubectl get deployment`|
List cluster replicasets|`kubectl get replicaset`|
List cluster statefulsets|`kubectl get statefulset`|
List all resources (deployments, services, replicasets, statefulsets, ...)|`kubectl get all`|
Delete servie|`kubectl delete service <service-name>`|
Delete deployment (and so the related pod)|`kubectl delete deployment <deploy-name>`|
Display cluster config as YAML|`kubectl config view`|
Serve a site to display APIs method, like `http://localhost:8001/api/v1/namespaces/default/pods/<PODNAME>/proxy/` or `http://localhost:8001/version`|`kubectl proxy`|

## Helm commands

Role|Command|Description
:-:|:-:|:-:
Initialize helm on cluster|`helm init`
Find all package on repo with specified names|`helm search <name1> <name2>`
Install a package|`helm install --name=<release-name> <package-name>`
Remove a package|`helm del --purge <release-name>`
List installed package|`helm list`
Update installed packages|`helm repo update`

## Minikube commands

Role|Command|Description
:-:|:-:|:-:
Create a single-node cluster named "Cluster01" and connect to this cluster|`minikube start -p <cluster-name>`
Create a single-node cluster with a specified ammount of memory & CPUs|`minikube start --cpus 4 --memory 8192`
Create endpoint for service "hello-node" and open site in a web browser|`minikube service <service-name>`
Open dashboard in web browser|`minikube dashboard`
Start a process that can provide external IP for cluster's services (not required from a cloud provider)|`minikube tunnel`
Enumerates all extensions (they are pre-build pods which provide services to extend cluster feature)|`minikube addons list`
Enable addon|`minikube addons enable <addon-name>`
Disable addon|`minikube addons disable <addon-name>`