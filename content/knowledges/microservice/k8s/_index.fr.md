---
title: "Kubernetes"
weight: 101
---

## Intro


### Présentation de Kubernetes

Kubernetes, aka K8S, permet de génrer les ressources d'un cluster, en voici quelques concepts :

* **BinPacking** : Attribution d'un score en fonction de l'usage d'un service afin d'adapter les ressource à lui reserver
* **Automated rollbacks** : Rollback auomatic d'un service si la mise à jour n'aboutie pas
* **Blue-Green deployments** : Mises à jour et rollback des solutions sans les rendre indisponible
* **ReplicaSets** : Duplication des services pour les rendre tolérantes au pannes et automatiser les mises à l'echelles horizontales
* **Self-healing** : Redemarrage automatique des services ou machines qui sont tombés
* **Affinity / anit-affinity** : Spécification de la VM sur lequelle le service doit se déployer
* **Service discovery & Load balancing**

![test](./_resources/test.drawio.svg)

### Avant de commencer 

* En tant que développeur, réspecter les règles de codage spécifiées dans les [12-factors apps](https://12factor.net/)
* En tant que développeur ou opérateur, renseignez-vous sur les outils existant pouvant répondre à vos besoin pour vous guider dans les choix technique
{{< cncfFrame >}}
