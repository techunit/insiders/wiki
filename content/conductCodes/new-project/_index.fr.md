---
title: "Initialisation d'un projet"
---

## Présenter votre projet
Afin d'initier correctement votre projet, il est indispensable que les membres comprennent les enjeux, vos attentes & éventuellement l'historique du besoin.
Afin de vous aider à présenter votre projet, voici le modèle à suivre et à publier dans le salon Discord.

* **Nom du projet**
* **Status & description du status**, le status peut être *En étude*, *Initié*, *En cours*, *En veille* ou *Clôturé*, exemples :
    * En cours : Développement en cours
    * En veille : En attente que le besoin revienne
    * Clôturé : L'opération s'est déroulée jusqu'au verdict du procès. La stratégie peut être utilisée pour d'autre cause si un membre souhaite s'en emparer.
* **Description des enjeux**, exemples :
    * S'essayer au développement d'un jeu et de tous les aspects qui en font partis
    * Mise en place des outils nécessaire à la communication et accès privé pour {{< techunit >}}
    * Gérer facilement les questions à poser à un intervenant lors d'un conférence Discord/Twitch
* **Description du fonctionnement**, exemples :
    * Solliciter des journalistes en public sur twitter pour les inciter à briser le silence qui règne autour de l'affaire Assange. La solution se définit par un site web d'information & 3 bots permettant d'organiser le serveur Discord *Asile Pour Assange*.
    * L'utilisateur interagit avec le bot pour choisir s'il veut rechercher un livre ou en ajouter un. Le bot contact l'utilisateur par MP afin de continuer la procédure de recherche ou d'ajout de livre. La bibliothèque est gérée sur un serveur Discord dédié.
    * Collecter des pièces informatiques destinés à être jeter pour les redistribuer
* **Description de la solution déterminée**